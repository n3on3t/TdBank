import requests
import time
from bs4 import BeautifulSoup
import csv

from beaver.scrapers.package.account import Account
from package import Scraper

class TDBank(Scraper, object):
    def __init__(self, driver="firefox", silent=True, headless=True):
        super(TDBank, self).__init__(driver, headless=headless, silent=silent)

    def get_session_cookies(self, username, password, securities):

        self.driver.get("https://easyweb.td.com/waw/idp/login.htm")
        self.driver.implicitly_wait(20)  # seconds
        self.driver.find_element_by_id("username100").send_keys(username)
        self.driver.find_element_by_id("password").send_keys(password)
        self.driver.find_element_by_xpath("/html/body/div[1]/div[2]/ui-view/challenge/ui-view/login-template/section[1]/div[1]/div[2]/div/div[1]/div/div/login-form/form/div/div/div[4]/div/div/button").click()
        time.sleep(15)
        if 'Accounts Overview' not in self.driver.title :
            print "MFA needed..."
            security_label = self.driver.find_element_by_xpath("//label[contains (@id,'labelWrap_302')]")
            if security_label :
                question = self.driver.find_element_by_xpath("//label[contains (@id,'labelWrap_302')]").text
                if securities.get(question) :
                    print "Here is the  answer :"+securities.get(question)
                    self.driver.find_element_by_id("answer").send_keys(securities.get(question))
                    self.driver.find_element_by_xpath("/html/body/div[3]/div[2]/section/div/div/form/div[3]/div/div/button").click()
                else :
                    print "there is no answer found following question is still unknown "+question
                print self.driver.current_url
                time.sleep(1)

        # check if we hit security question
        # if self.driver.current_url.startswith('https://easyweb.td.com/waw/idp/authenticate.htm'):
        #     print "repondre a question"
        #     self.driver.find_element_by_id(
        #         "MFAChallengeForm:answer").send_keys(security_answer)
        #     self.driver.find_element_by_id("MFAChallengeForm:next").submit()
        #
        # self.driver.save_screenshot('screenie_logged.png')
        while not self.driver.current_url.startswith('https://easyweb.td.com/waw/ezw/webbanking'):
            print self.driver.current_url
            time.sleep(1)

        time.sleep(1)
        ##allow time for cookies to initialize
        cookies = ['{}={}'.format(cookie['name'], cookie['value']) for cookie in self.driver.get_cookies()]
        self.cookies = '; '.join(cookies)


    def find_accounts(self):
        headers = {
            'Host': "easyweb.td.com",
            'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0",
            'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            'Accept-Language': "en-US,en;q=0.5",
            'Referer': "https://easyweb.td.com/waw/ezw/servlet/ca.tdbank.banking.servlet.AccountDetailsServlet",
            'Content-Type': "application/x-www-form-urlencoded",
            'Connection': "keep-alive",
            'Upgrade-Insecure-Requests': "1",
            'Cache-Control': "no-cache",
            'Cookie': self.cookies
        }
        url = 'https://easyweb.td.com/waw/ezw/servlet/ca.tdbank.banking.servlet.FinancialSummaryServlet?type=PRXP'
        accounts = []
        resp = requests.get(url, headers=headers)
        soup = BeautifulSoup(resp.text, 'html.parser')
        #banking_section = self.driver.find_element_by_xpath("//div[contains(@class, 'td-target-banking')]//h5[contains (@class,'td-margin-top-none')]")

        banking_section =  soup.select("div.td-target-banking h5.td-margin-top-none")[0].text

        #cards_section = self.driver.find_element_by_xpath("//div[contains(@class, 'td-target-creditcards')]//h5[contains (@class,'td-margin-top-none')]")
        cards_section =  soup.select("div.td-target-creditcards h5.td-margin-top-none")[0].text

        #investment_section = self.driver.find_element_by_xpath("//div[contains(@class, 'td-target-investing')]//h5[contains (@class,'td-margin-top-none')]")
        investment_section =  soup.select("div.td-target-investing h5.td-margin-top-none")[0].text

        if 'Banking' in banking_section :
            print "There is a banking section"
            for b in soup.select('div.td-target-banking tr') :
                if not b.has_attr("class"):
                    banking_number = b.select("span.td-copy-nowrap")[0].text
                    banking_amount = b.select("td.td-copy-align-right")[0].text.strip()

                    banking_type = b.select("a")[0].text.strip()
                    account = Account(banking_number,banking_type,banking_amount)
                    accounts.append(account)
        if 'Credit Cards' in cards_section :
            print "There is a Credit Cards section"
            for b in soup.select("div.td-target-creditcards tr") :
                if not b.has_attr("class"):
                    banking_number = b.select("span.td-copy-nowrap")[0].text
                    banking_amount = b.select("td.td-copy-align-right")[0].text.strip()
                    banking_type = b.select("a")[0].text.strip()
                    account = Account(banking_number, banking_type, banking_amount)
                    accounts.append(account)
        if 'Investments' in investment_section :
            print "There is a Investments section"
            for b in soup.select("div.td-target-investing tr") :
                if not b.has_attr("class"):
                    banking_number = b.select("span.td-copy-nowrap")[0].text
                    banking_amount = b.select("td.td-copy-align-right")[0].text.strip()
                    banking_type = b.select("a")[0].text.strip()
                    account = Account(banking_number, banking_type, banking_amount)
                    accounts.append(account)


        self.driver.close()

    def get_transaction_csv(self, account, start_date, end_date):
        url = 'https://easyweb.td.com/waw/ezw/servlet/ca.tdbank.banking.servlet.DownloadAccountActivityServlet'
        payload = {
            'selaccounts': account,
            'DateRange': 'CTM',
            'PFM': 'csv',
            'xptype': 'PRXP',
            'actiontaken': 'D',
            'referer': 'AA',
            'commingfrom': 'AA',
            'ExprtInfo': '',
            'fromDate': '{start_date.year}-{start_date.month}-{start_date.day}',
            'toDate': '{end_date.year}-{end_date.month}-{end_date.day}',
            'filter': 'f1'
        }
     
        headers = {
            'Host': "easyweb.td.com",
            'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0",
            'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            'Accept-Language': "en-US,en;q=0.5",
            'Referer': "https://easyweb.td.com/waw/ezw/servlet/ca.tdbank.banking.servlet.AccountDetailsServlet",
            'Content-Type': "application/x-www-form-urlencoded",
            'Connection': "keep-alive",
            'Upgrade-Insecure-Requests': "1",
            'Cache-Control': "no-cache",
            'Cookie': self.cookies
        }

        with requests.Session() as s:
            download = s.post(url, data=payload, headers=TDBank.headers)

            decoded_content = download.content.decode('utf-8')

            cr = csv.reader(decoded_content.splitlines(), delimiter=',')
            out_csv = list(cr)
            if len(out_csv[0]) != 5:
                raise Exception('Could not load transactions for {year}, {month}')
            return out_csv

    def get_credit_transactions(self, account, cycleId):
        url = "https://easyweb.td.com/waw/api/account/creditcard/download"

        querystring = {"accountKey": account,"cycleId": str(cycleId), "downloadAccountFormat":"CSV"}

        headers = {
            'Host': "easyweb.td.com",
            'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0",
            'Accept': "application/json, text/plain, */*",
            'Accept-Language': "en-US,en;q=0.5",
            'Referer': "https://easyweb.td.com/waw/exp/",
            'Connection': "keep-alive",
            'Cache-Control': "no-cache",
            # 'Cookie': self.cookies,
            'Cookie': "",
        }

        with requests.Session() as s:
            resp = s.get(url, headers=headers, params=querystring)

            decoded_content = resp.content.decode('utf-8')

            cr = csv.reader(decoded_content.splitlines(), delimiter=',')
            out_csv = list(cr)
            if len(out_csv[0]) != 5:
                raise Exception('Could not load transactions for {cycleId}')
            return out_csv


