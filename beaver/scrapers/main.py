from tdbank import TDBank
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('ConfigFile.properties')
bank = TDBank()

card_number = config.get('CredentialsSection', 'credential.card_number');
password = config.get('CredentialsSection', 'credential.password');

securities = {}
securities[config.get('SecuritySection', 'security.question1')] = config.get('SecuritySection', 'security.answer1')

bank.get_session_cookies(card_number,password,securities)
print 'trying to connect'
print bank.find_accounts()